const nodemailer = require("nodemailer");

const mailTransporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: false, // true for 465, false for other ports
    auth: {
      user: process.env.MAIL_USER, // generated ethereal user
      pass: process.env.MAIL_PASS, // generated ethereal password
    },
  });

  // sendMail -> sends mail with the initlised transporter
  const sendMail = (mailOptions) => {
    return new Promise((resolve, reject)=>{
        mailTransporter.sendMail(mailOptions)
        .then(info => resolve(info))
        .catch(err=>reject(err));
    });
  }
  module.exports = { mailTransporter, sendMail }
