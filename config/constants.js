module.exports = {
    allocated: "Allocated",
    waitlist: "Waitlist",
    standBy: "Standby",
    cancelled: "Cancelled"
}