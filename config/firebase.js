const admin = require("firebase-admin");

admin.initializeApp({
    credential: admin.credential.cert(require('../serviceAccountKey.json')),
    databaseURL: process.env.FIREBASE_DATABASE_URL,
}, );

// console.log("App Name:", admin.app().name)
const firestore = admin.firestore();
module.exports =  { admin, firestore };