
const CONSTANTS = require("../config/constants");
const tableTypes = [CONSTANTS.allocated, CONSTANTS.waitlist, CONSTANTS.standBy, CONSTANTS.cancelled];

// getFilteredDataObject -> filters the data based on status type
const getFilteredDataObject = (data, statusType) => {
    return data.filter((res) => {
        if (res.statusType == statusType) {
            return res;
        }
    });
};

// getCSVGroupData -> csv data on group with categories
const getCSVGroupData = (bookingData) => {
    const tableColumnHeaders = ["Start Time", "End Time", "Next Booking", "Name", "Pax", "Children", "High Chair", "Level", "Customer Preference", "Menu", "Table", "Area Preference", "Staff Note", "Customer Note", "Butler Prepay", "Butler Preoder", "Booking Payments", "Allergies", "Diatery Requirements", "Occassion"];
    const finalData = [];
    tableTypes.forEach((tableType) => {
        const dataCounts = getFilteredDataObject(bookingData, tableType);
        if (dataCounts.length != 0) {
            finalData.push([tableType], tableColumnHeaders);
            bookingData.map((res) => {
                if (res.statusType == tableType) {
                    finalData.push([
                        res.start_time,
                        res.end_time,
                        res.next_booking,
                        res.name,
                        res.pax,
                        res.children,
                        res.high_chair,
                        res.level,
                        res.customer_preference,
                        res.menu,
                        res.table,
                        res.area_preferences,
                        res.staff_note,
                        res.customer_note,
                        res.butler_prepay,
                        res.butler_pre_order,
                        res.booking_payments,
                        res.allergies,
                        res.diatery_requirement,
                        res.occassion
                    ]);
                }
            });
            finalData.push([]);
        }
    });
    return finalData;
}

// getPdfRequiredTableData -> pdf response data format 
const getPdfRequiredTableData = (bookingData) => {
    const responseObj = {};
    tableTypes.forEach((tableType) => {
        const datas = getFilteredDataObject(bookingData, tableType);
        if (datas.length != 0) {
            responseObj[tableType] = datas;
        }
    });
    return responseObj;
}



module.exports = {
    getCSVGroupData,
    getPdfRequiredTableData
}