
const { stringify } = require("csv")
const ejs = require('ejs');
const puppeteer = require('puppeteer')
const moment = require("moment");
const CONSTANTS = require("../config/constants");
const _ = require("lodash");



// generateCSV -> will gnerate the csv in given location
exports.generateCSV = (data) => {
    return new Promise((resolve, reject) => {
        stringify(data, { header: false }, (err, output) => {
            if (err) { reject(err); return; }
            resolve(output);
        });
    });
}

// renderHTML -> renders the given file
exports.renderHTML = (templatePath, params) => {
    return new Promise(async (resolve, reject) => {
        try {
            const data = await ejs.renderFile(templatePath, params);
            resolve(data);
        } catch (error) {
            console.log("Error ::", JSON.stringify(error));
            reject(error)
        }
    });
}

// generateHTMLtoPDF -> generates the pdf buffer on given html
exports.generateHTMLtoPDF = async (html) => {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.setContent(html, { waitUntil: 'domcontentloaded' });
    const pdfBuffer = await page.pdf({ format: 'A4', landscape: true });
    await browser.close();
    return pdfBuffer;
}

// utils ,
const utils = {
    anyIntersection: (obj1, obj2) => {
        if (obj1 && obj2) {
            const length1 = Object.keys(obj1).length;
            const length2 = Object.keys(obj2).length;
            const compare1 = length1 <= length2 ? obj1 : obj2;
            const compare2 = length1 <= length2 ? obj2 : obj1;
            for (const k in compare1) {
                if (compare2[k]) {
                    return true;
                }
            }
        }
        return false;
    }
}

// formatCollectionData -> get next booking
const getNextBooking = (bookingId, bookings) => {
    const files = {
        statuses: {
            "confirmed": { "text": "Confirmed", "value": 1 },
            "arrived": { "text": "Arrived", "value": 2 },
            "seated": { "text": "Seated", "value": 4 },
            "vacated": { "text": "Vacated", "value": 8 },
            "cancelled": { "text": "Cancelled", "value": 16 },
            "noShow": { "text": "No Show", "value": 32 }
        },
        functionStatus: {
            "inProgress": { "text": "In Progress", "value": 1, "category": "Enquiry" },
            "depositRequested": { "text": "Deposit Requested", "value": 2, "category": "Enquiry" },
            "confirmed": { "text": "Confirmed", "value": 3, "category": "Confirmed" },
            "vacated": { "text": "Vacated", "value": 4, "category": "Confirmed" },
            "lost": { "text": "Lost", "value": 5, "category": "Cancelled" },
            "denied": { "text": "Denied", "value": 6, "category": "Cancelled" },
            "discardedDate": { "text": "Discarded Date", "value": 7, "category": "Cancelled" },
            "cancelled": { "text": "Cancelled", "value": 8, "category": "Cancelled" }
        }
    };

    const booking = _.find(bookings, (b) => b._key === bookingId && Number(b.status) !== files.statuses.cancelled.value);
    if (booking && booking.intervalId) {
        let maxDate = Number.MAX_SAFE_INTEGER;
        for (let i = 0; i < bookings.length; i++) {
            const booking1 = _.find(bookings, (b) => b._key === bookings[i]._key && Number(b.status) !== files.statuses.cancelled.value);
            if (booking1 && booking1.intervalId) {
                if (bookingId !== bookings[i]._key && booking.intervalId < booking1.intervalId && booking1.intervalId < maxDate && utils.anyIntersection(booking1.alg.tables, booking.alg.tables)) {
                    maxDate = Number(booking1.intervalId);
                }
            }
        }
        if (maxDate !== Number.MAX_SAFE_INTEGER) {
            return moment(maxDate).utc().format('hh:mmA');
        }
    }
    return '';
}

// formatCollectionData -> formats the data
exports.formatCollectionData = (data) => {
    return Object.values(data).map((res) => {
        const obj = {
            start_time: (res.alg && res.alg.time) ? moment(res.alg.time).format('hh:mm A') : '',
            end_time: (res.alg && res.alg.time && res.alg.duration) ? moment(res.alg.time).add(res.alg.duration, 'minutes').format('hh:mm A') : '',
            next_booking: getNextBooking(res._id, Object.values(data)),
            name: res.customer ? res.customer.name : '',
            pax: res.pax,
            children: res.children || '',
            high_chair: res.highChair,
            level: res.customer.level,
            customer_preference: '',
            menu: '',
            table: (res.alg && res.alg.number) ? res.alg.number : '',
            area_preferences: '',
            staff_note: res.staffNote || '',
            customer_note: res.customerNote || '',
            butler_prepay: '',
            butler_pre_order: '',
            allergies: res.customer.allergies || '',
            occassion: res.customer.occassionId || '',
            stand_by: res.stand_by || '',
            date: moment(res.date).format("YYYY-MM-DD"),
            meal: res.mealId,
            status: res.status,
            courseId: res.courseId,
            group_area_id: res.groupAreaId,
            causal: res.causal,
            locked: res.locked,
            widget: res.widgetId,
            created_date: moment(res.dateCreated).format("YYYY-MM-DD"),
            use_alternative_table: res.useAlternativeTables,
            guests: res.guests && Array.isArray(res.guests) ? res.guests.length : '',
            name: res.customer.name,
            diatery_requirement: res.customer.dietaryRequirements,
            covid_19_decleared: res.customer.covid19Decleared,
            mobile: res.customer.mobile,
            email: res.customer.email,
            company: res.customer.company,
            booking_payments: ''

        };
        if (res.butlerPreferences) {
            obj.wine_preference_id = res.butlerPreferences.winePreferenceId || '';
            obj.dessert_message = res.butlerPreferences.dessertMessage || '';
            obj.special_butler_preferences_request = res.butlerPreferences.specialButlerPreferencesRequest || '';
        }

        if ([null, 0, 1, 2, 4].includes(res.status)) {
            obj.statusType = CONSTANTS.allocated;
        }
        if (res.status && res.status == 16) {
            obj.statusType = CONSTANTS.cancelled;
        }
        if (res.status && res.alg && res.status == res.alg.waitlist) {
            obj.statusType = CONSTANTS.waitlist;
        }
        if ([1, 2].includes(res.standby)) {
            obj.statusType = CONSTANTS.standBy;
        }
        return obj;
    });
}

