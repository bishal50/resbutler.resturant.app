var express = require('express');
var router = express.Router();
var _ = require("lodash");

var { sendMail } = require("../config/mail")
var { getContactData, getRestaurantData } = require("../models/contacts")
var { generateCSV, renderHTML, generateHTMLtoPDF, formatCollectionData } = require("../utils/index")
var { getCSVGroupData, getPdfRequiredTableData } = require("../utils/dataFormater")
var moment = require("moment");
const { default: axios } = require('axios');
var { files } = require('resbutler-utils');
var { firestore, admin } = require('../config/firebase')



// handleProcessBooking -> sends the csv and pdf as attachement as enail out from firestore
// date : 20210816
// restaurantId: -Kc_XU6h-0pCMGdFFByF
// mealId: -MNXa8yrkSlaXPF5iKQd
const handleProcessBooking = async (req, res) => {
  console.log(req.query)
  try {
    if (!_.get(req, 'query.restaurantId', false)) {
      return res.status(400).json({
        "resturantId": "Required"
      });
    }

    if (!_.get(req, 'query.date', false)) {
      return res.status(400).json({
        "date": "Required"
      });
    }

    const normalTemplate = await renderHTML(
      `${__dirname}/../views/normal.ejs`, {
      username: process.env.MAIL_TO_USERNAME
    });
    const contactCollection = await getContactData(req.query);
    const formatedData = formatCollectionData(contactCollection); //returns an object
    const sortedData = _.sortBy(formatedData, "start_time", ['asc']); //arrange based on time

    const csvArrayData = getCSVGroupData(sortedData);
    const csvData = await generateCSV(csvArrayData);

    const pdfData = getPdfRequiredTableData(sortedData);
    const resturantInfo = await getRestaurantData(req.query.restaurantId);
    const renderPDFHTML = await renderHTML(
      `${__dirname}/../views/booking-information.ejs`, {
      restaurant: resturantInfo,
      date: moment(req.date).format("YYYY-MM-DD"),
      data: pdfData
    });
    const htmlPDF = await generateHTMLtoPDF(renderPDFHTML);
    const mailOptions = {}
    mailOptions.from = process.env.MAIL_FROM;
    mailOptions.to = process.env.MAIL_TO_EMAIL;
    mailOptions.subject = "Booking information";
    mailOptions.html = normalTemplate;
    mailOptions.attachments = [{
      filename: 'Booking Information_CSV Export.csv',
      content: csvData,
      contentType: "text/csv"
    },
    {
      filename: "Booking Information_PDF Export.pdf",
      content: htmlPDF,
      contentType: "application/pdf"
    }
    ];
    await sendMail(mailOptions)
    res.json({
      success: true,
    })
  } catch (error) {
    console.log("err ::", error)
    res.json(error)

  }
}


// routers
router.get("/process-booking", handleProcessBooking);
router.post("/run-static", async (req, res) => {
  try {
    const response = await axios.post(
      'https://australia-southeast1-staging-60983.cloudfunctions.net/bookings/rerunStatic',
      {
        restaurantId: "-MYrBW_cgZSMFKwbNtM-",
        mealId: "-MnxX4yJwjL-G_2ghNcQ",
        date: "20211117"
      },
      {
        headers: {
          'client-id': '7b0418b0-f375-4300-bf6b-348cd559bd47',
          'token': 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjJlMzZhMWNiZDBiMjE2NjYxOTViZGIxZGZhMDFiNGNkYjAwNzg3OWQiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiU3lzdGVtIFNhbmRib3giLCJyb2xlcyI6MzIsInRlbmFudHMiOnsic3RhZ2luZy1zYW5kYm94Ijp0cnVlfSwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL3N0YWdpbmctNjA5ODMiLCJhdWQiOiJzdGFnaW5nLTYwOTgzIiwiYXV0aF90aW1lIjoxNjM3NzU2NzA2LCJ1c2VyX2lkIjoiY2FnY0VHMjRFMldkUFRNRFpIa3BZenJpS0thMiIsInN1YiI6ImNhZ2NFRzI0RTJXZFBUTURaSGtwWXpyaUtLYTIiLCJpYXQiOjE2Mzc3NTczMTIsImV4cCI6MTYzNzc2MDkxMiwiZW1haWwiOiJzeXN0ZW1fc2FuZGJveEByZXNidXRsZXIuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInBob25lX251bWJlciI6Iis2MTQwMTk4ODg4OCIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsicGhvbmUiOlsiKzYxNDAxOTg4ODg4Il0sImVtYWlsIjpbInN5c3RlbV9zYW5kYm94QHJlc2J1dGxlci5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJjdXN0b20ifX0.Bsw1TTgEfe9fZNcKMCHHUUJB68roMLYv1kZhNLkYAT3blQtLPFw2RUg6CaBltTgPbb-L9kDgv8VABTLWr4ljBHqTR0JMeNFOPKACuKZTB-Ffx-Bk8d8Z_rtwqzlobDZ2lRUwYyfZf2c_RfDXZ1_HK3cU0rog-0xPpFZlUl7cGEKetVWIdhIr8frVo20wyaqCxEeMVHMaw5Z4EqGAmAF7ETEog6HaAzyPHI2dkjmYUJW9nPBedJttAqtg4eXSNFv2K2dTqo8OfwYHFyJ3L7p5BmHX8PSDf0ZpTTGBL8XgPFhkPGPQEthpIjyulVfdPnYOFnWUfo87bmJnNUxjFqEdWg'
        }
      });

    const bookings = _.values(response.data.bookings)
    const allocatedBookings = _.filter(bookings, (b) => b.status !== files.statuses.noShow.value && b.status !== files.statuses.cancelled.value && !b.alg.waitlist && !b.alg.onHold && !b.alg.setupUnallocated && !b.alg.supervipUnallocated)

    const promises = []
    allocatedBookings.map((booking) => {
      promises.push(
        firestore.doc(`1a97d95d-4c0a-451e-92fa-46a1135f8134/bookings/allocatedBookings/${booking._key}`).set(booking)
      )
    })
    await Promise.all(promises);
    res.json(allocatedBookings)

  } catch (error) {
    console.log(error)
    res.json(error)
  }
});

router.post("/feedbacks/sendFeedbackResponse", async (req, res) => {
  try {
    const clientId = req.headers[`client-id`];
    const { commentId, feedbackId, commentId, customer, sendMessage, createdAt } = req.body;
    const collection = `${clientId}/panorama/feedbacks/${feedbackId}/responses`;
    await firestore.collection(collection).doc(commentId).update({
      messages: admin.firestore.FieldValue.arrayUnion({
        sendMessage,
        createdAt,
        id: firestore.collection(collection).doc(commentId).id, // 
        status: 'sent',
      }),
      updatedAt: firebase.database.ServerValue.TIMESTAMP
    });
    // sends message to the customer via sms or other messaging services. use customer object to retrive the customer information to send messages to customer. 
  } catch (error) {
    console.log(error)
    res.json(error)
  }
})

router.get('/', (req, res, next) => res.render('index', { title: 'Health-check OK' })); //health-check
module.exports = router;
