# Resturant App

1. Create a function to read the provided service account file.
2. From this file, you should be able to get ResButler bookings data
3. Create a process to export the bookings data (for a specific client, restaurant, meal period) as a CSV and PDF via Google Cloud Platform
4. Create a process to send the bookings data to an email

## Development
- Areate .env file references from .env.example
- Add firebase `serviceAccountKey.json` in the root of project

## Installation and start 
```
yarn install && yarn start
```

| Request                      | Method | Description                        |
| ---------------------------- | -------|----------------------------------- |
| `{site-name}/process-booking`| `get`  | perform above process              |
