const { firestore } = require("../config/firebase");

const getContactData = async (queryParams) => {
    let collection = firestore.collection("1a97d95d-4c0a-451e-92fa-46a1135f8134/bookings/bookings")
    if (queryParams.date) {
        collection = collection.where("date", "==", queryParams.date)
    }
    if (queryParams.restaurantId) {
        collection = collection.where("restaurantId", "==", queryParams.restaurantId)
    }
    if (queryParams.mealId) {
        collection = collection.where("mealId", "==", queryParams.mealId)
    }
    const snap = await collection.get();
    let data = {}
    for (const doc of snap.docs) {
        const singleData = doc.data();
        singleData._id = doc.id;
        singleData.customer = {}
        if (singleData.customerId) {
            const customerObj = await getCustomerData(singleData.customerId);
            if (customerObj) {
                singleData.customer = {
                    name: `${customerObj.firstName} ${customerObj.lastName}`,
                    email: customerObj.email,
                    mobile: customerObj.mobile,
                    level: customerObj.level,
                    covid19Decleared: customerObj.covid19Declared,
                    allergies: customerObj.allergies,
                    dietaryRequirements: customerObj.dietaryRequirements,
                    company: customerObj.company,
                    occasionId: customerObj.occasionId,
                };
            }
        }
        data = Object.assign(data, { [doc.id]: singleData });
    };
    return data;
}


const getCustomerData = async (customerId) => {
    let snap = await firestore.doc(`1a97d95d-4c0a-451e-92fa-46a1135f8134/crm/customers/${customerId}`).get();
    let data = snap.data();
    return data;
}

const getRestaurantData = async (restaurantId) => {
    let snap = await firestore.doc(`1a97d95d-4c0a-451e-92fa-46a1135f8134/restaurants/restaurants/${restaurantId}`).get();
    let data = snap.data();
    return data;
}

module.exports = {
    getContactData,
    getRestaurantData
}